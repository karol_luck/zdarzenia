<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
define('BACKEND_DIR', '../backend/');

function __autoload($className) {
    if(file_exists(BACKEND_DIR . "controller/$className.php")) $classDir = 'controller';
    else if((file_exists(BACKEND_DIR . "class/$className.php"))) $classDir = 'class';
    else if((file_exists(BACKEND_DIR . "model/$className.php"))) $classDir = 'model';
    else if((file_exists(BACKEND_DIR . "interface/$className.php"))) $classDir = 'interface';
    else throw new Exception("Class file not exists ($className).");

    require_once(BACKEND_DIR . "$classDir/$className.php");
}

if(!isset($_GET['controller'])) throw new Exception("Controller not set");

$controllerName = ucfirst($_GET['controller']) . 'Controller';
$action = $_GET['action'];
if($action == 'auto' && !empty($_GET['method'])) {
    $action = strtolower($_GET['method']);
}
$actionName = $action . 'Action';

$controller = new $controllerName();
$controller->$actionName();
