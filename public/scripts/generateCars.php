<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
define('BACKEND_DIR', '../../backend/');

include(BACKEND_DIR . 'interface/IDataBase.php');
include(BACKEND_DIR . 'class/MySql.php');

$db = new MySql();

$data = json_decode(file_get_contents('data/cars.json'), true);
$brands = $data['carslist'];

// Zwraca pseudolosowy numer tablicy rejestracyjnej
function randomReg() {
    $chars = array(
        rand(65, 90),
        rand(65, 90),
        rand(65, 90)
    );
    return chr($chars[0]).chr($chars[1]).chr($chars[2]).' '.substr(strtoupper(md5(rand(1,10000))), 5, 4);
}

for($i = 1; $i <= 10; $i++) {
    $brandIdx = rand(0, count($brands)) - 1;
    if(empty($brands[$brandIdx])) {
        $i--;
        continue;
    }
    $brand = $brands[$brandIdx];
    $brandName = $brand['carname'];

    $cars = $brand['carmodellist']['carmodel'];
    $carIdx = rand(0, count($cars)) - 1;
    if(empty($cars[$carIdx])) {
        $i--;
        continue;
    }
    $carName = $cars[$carIdx];
    $carReg = randomReg();

    $db->getDriver()->query("INSERT INTO `cars` (brand, model, regNumber) VALUES ('$brandName', '$carName', '$carReg')");
}