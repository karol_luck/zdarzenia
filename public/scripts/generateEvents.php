<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
define('BACKEND_DIR', '../../backend/');

include(BACKEND_DIR . 'interface/IDataBase.php');
include(BACKEND_DIR . 'class/MySql.php');

$db = new MySql();

// Plik z google directions, punkty lat lng na 100% wskazują więc na miejsce na drodze w polsce
$directions = file_get_contents('data/directions.json');
// Wyrażenie regularne wyłuskujące pary lat lng z pliku directions
$latLngRegex = '/"lat" : ([0-9\.]*),\s* "lng" : ([0-9\.]*)/s';

$matches = array();
preg_match_all($latLngRegex, $directions, $matches);

$latArr = $matches[1];
$lngArr = $matches[2];

$eventsTypes = array('W', 'K', 'T', 'I');

$usersCnt = $db->getDriver()->query("SELECT COUNT(*) AS total FROM `users`")->fetch();
$carsCnt = $db->getDriver()->query("SELECT COUNT(*) AS total FROM `cars`")->fetch();

$toAdd = rand(1, 3);
if($toAdd == 3) $toAdd = rand(2, 3);
for($i = 1; $i <= $toAdd; $i++) {
    $idx = rand(1, count($latArr)) - 1;
    $lat = $latArr[$idx];
    $lng = $lngArr[$idx];
    $typeIdx = rand(0, 3);
    $type = $eventsTypes[$typeIdx];
    $userId = rand(1, $usersCnt['total']);
    $carId = rand(1, $carsCnt['total']);
    $unixTime = time();
    //$unixTime = rand(strtotime("2013-01-01"), time());

    $db->getDriver()->query("INSERT INTO `events` (`date`, `lat`, `lng`, `type`, `carId`, `userId`) VALUES
                                (FROM_UNIXTIME($unixTime), {$lat}, {$lng}, '$type', $carId, $userId)");
}