Ext.define('App.store.Events', {
    extend: 'Ext.data.Store',
    model: 'App.model.Event',
    autoLoad: true,
    proxy: {
        type: 'ajax',
        url: '/rest/events/auto',
        reader: {
            type: 'json',
            root: 'events',
            totalProperty: 'total'
        }
    }
});
