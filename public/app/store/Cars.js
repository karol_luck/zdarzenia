Ext.define('App.store.Cars', {
    extend: 'Ext.data.Store',
    model: 'App.model.Car',
    autoLoad: true,
    pageSize: 20,
    proxy: {
        type: 'ajax',
        url: '/rest/cars/auto',
        reader: {
            type: 'json',
            root: 'cars',
            totalProperty: 'total'
        }
    }
});
