Ext.define('App.store.Users', {
    extend: 'Ext.data.Store',
    model: 'App.model.User',
    autoLoad: true,
    pageSize: 20,
    proxy: {
        type: 'ajax',
        url: '/rest/users/auto',
        reader: {
            type: 'json',
            root: 'users',
            totalProperty: 'total'
        }
    }
});
