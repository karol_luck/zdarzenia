Ext.define('App.controller.WithPages', {
    extend: 'Ext.app.Controller',

    getMyStore: function() { console.log('restStore is not implemented!'); },

    init: function() {
        var me = this,
            store = this.getMyStore(),
            container = this.getContainerXType();

        var controlObj = {};
        controlObj[container + ' #page-increase'] = {
            click: function() { me.getPageNumber().setValue( parseInt(me.getPageNumber().getValue()) + 1 ); }
        };
        controlObj[container + ' #page-decrease'] = {
            click: function() { me.getPageNumber().setValue( parseInt(me.getPageNumber().getValue()) - 1 ); }
        };
        controlObj[container + ' #page-forward'] = {
            click: function() { me.getPageNumber().setValue( parseInt(me.getPageNumber().getValue()) + 5 ); }
        };
        controlObj[container + ' #page-backward'] = {
            click: function() { me.getPageNumber().setValue( parseInt(me.getPageNumber().getValue()) - 5 ); }
        };
        controlObj[container + ' #page-first'] = {
            click: function() { me.getPageNumber().setValue( 1 ); }
        };
        controlObj[container + ' #page-last'] = {
            click: function() { me.getPageNumber().setValue( Math.ceil(store.getTotalCount() / store.pageSize) ); }
        };
        controlObj[container + ' #page-number'] = {
            change: function(item, newValue) {
                var lastPage = Math.ceil(store.getTotalCount() / store.pageSize)
                if(!newValue || !parseInt(newValue) || newValue < 1) {
                    item.setValue(1);
                }
                else if(newValue > lastPage) {
                    item.setValue(lastPage);
                }
                else {
                    me._loadStorePage(parseInt(newValue));
                }
            }
        };

        this.control(controlObj);
    },

    _loadStorePage: function(page) {
        var me = this,
            store = this.getMyStore();

        page = page || store.currentPage;

        store.loadPage(page, {
            callback: function() {
                var lastPage = Math.ceil(store.getTotalCount() / store.pageSize);
                if(store.currentPage > lastPage) {
                    me.getModal().down('#page-number').setValue(lastPage);
                }
                else {
                    me._updateNav();
                    me._setItemBtns();
                }
            }
        });
    },

    _updateNav: function() {
        var btnPrev = this.getModal().down('#page-decrease'),
            btnNext = this.getModal().down('#page-increase'),
            btnBackward = this.getModal().down('#page-backward'),
            btnForward = this.getModal().down('#page-forward'),
            btnFirst = this.getModal().down('#page-first'),
            btnLast = this.getModal().down('#page-last'),
            store = this.getMyStore(),
            lastPage = Math.ceil(store.getTotalCount() / store.pageSize);

        btnPrev.setDisabled(store.currentPage <= 1);
        btnBackward.setDisabled(store.currentPage <= 1);
        btnFirst.setDisabled(store.currentPage <= 1);
        btnNext.setDisabled(store.currentPage >= lastPage);
        btnForward.setDisabled(store.currentPage >= lastPage);
        btnLast.setDisabled(store.currentPage >= lastPage);
    },

    _setItemBtns: function(enabled) {}
});