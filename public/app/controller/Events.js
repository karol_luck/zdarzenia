Ext.define('App.controller.Events', {
    extend: 'App.controller.WithPages',
    views: [],
    stores: ['Events'],

    refs: [
        {ref: 'selectedDetails', selector: 'selectedDetails'},
        {ref: 'selectedMap', selector: 'gmappanel'},
        {ref: 'deleteBtn', selector: '#deleteBtn'},
        {ref: 'eventsList', selector: 'eventsList'},
        {ref: 'modal', selector: 'eventsList'}
    ],

    mapPos: {lat: 54.3500, lng: 18.6667, def: true},

    getContainerXType: function() {
        return 'eventsList';
    },
    getPageNumber: function() {
        return this.getEventsList().down('#page-number');
    },
    getMyStore: function() {
        return this.getStore('Events');
    },

    lastEventId: null,

    init: function() {
        var me = this;

        Ext.TaskManager.start({
            run: this.checkEvents,
            interval: 10000,
            scope: me
        });
        this.control({
            // Lista zdarzeń
            'eventsList': {
                itemclick: function(dv, record) {
                    me.selectEvent(record);
                }
            },

            // Szczegóły wybranego zdarzenia
            '#selectedTab': {
                tabchange: function(tabPanel, newCard) {
                    var idx = tabPanel.items.findIndex('id', newCard.id);
                    if(idx == 1) {
                        me._setMapPoint();
                    }
                }
            },

            '#deleteBtn': {
                click: function() {
                    var me = this;
                    Ext.MessageBox.confirm('Uwaga', 'Czy na pewno chcesz usunąć wybrane zdarzenie?', function(choice) {
                        if(choice == 'yes') {
                            me.confirmDelete();
                        }
                    });
                }
            }
        });
        this.callParent();
    },

    confirmDelete: function() {
        var me = this;
        var record = this.getEventsList().getSelectionModel().getSelection()[0];
        record.destroy({
            callback: function() {
                me._loadStorePage();
            }
        });
    },

    checkEvents: function() {
        var me = this;
        Ext.Ajax.request({
            url: '/rest/events/lastEvent',
            success: function(response) {
                var obj = Ext.decode(response.responseText);
                if(me.lastEventId && me.lastEventId != obj.lastId) {
                    me._loadStorePage();
                }
                me.lastEventId = obj.lastId;
            }
        });
    },

    selectEvent: function(record) {
        var delBtn = this.getDeleteBtn();
        if(!record) {
            record = {"get": function() {return '---';}}
            delBtn.setDisabled(true);
        }
        var selected = this.getSelectedDetails();
        selected.down('#eventDate').setText(record.get('date'));
        selected.down('#eventType').setText(this._eventType(record.get('type')));
        selected.down('#eventLat').setText(this._eventType(record.get('lat')));
        selected.down('#eventLng').setText(this._eventType(record.get('lng')));
        selected.down('#eventCarBrand').setText(this._eventType(record.get('carBrand')));
        selected.down('#eventCarModel').setText(this._eventType(record.get('carModel')));
        selected.down('#eventCarReg').setText(this._eventType(record.get('carReg')));
        selected.down('#eventUserName').setText(this._eventType(record.get('userName')));
        selected.down('#eventUserSurname').setText(this._eventType(record.get('userSurname')));

        if(record.get('lat') == '---' || record.get('lng') == '---') {
            this._setMapPoint({}, true);
            delBtn.setDisabled(true);
        }
        else {
            this._setMapPoint({lat: record.get('lat'), lng: record.get('lng')});
            delBtn.setDisabled(false);
        }
    },

    _setItemBtns: function(enabled) {
        this.selectEvent(null);
    },

    _setMapPoint: function(newMapPos, resetPos) {
        if(newMapPos) {
            this.mapPos = newMapPos;
        }
        if(resetPos) {
            this.mapPos = {lat: 54.3500, lng: 18.6667, def: true};
        }
        var map = this.getSelectedMap();
        var googleMap = map.getMap();
        map.clearMarkers();
        if(!this.mapPos.def) map.addMarker(this.mapPos, {});
        googleMap.setCenter(this.mapPos);
    },

    _eventType: function(short) {
        // TODO: throw out to store
        var eventTypes = {
            W: 'Wypadek',
            K: 'Kolizja',
            T: 'Problem techniczny',
            I: 'Inny'
        };
        if(eventTypes[short]) return eventTypes[short];
        return short;
    }
});
