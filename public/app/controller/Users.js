Ext.define('App.controller.Users', {
    extend: 'App.controller.BaseCrud',
    views: ['Users.MainWindow', 'Users.Form'],
    stores: ['Users'],

    refs: [
        {ref: 'modal', selector: 'UsersModal', xtype: 'usersModal', autoCreate: true},
        {ref: 'form', selector: 'UsersForm', xtype: 'usersForm', autoCreate: true}
    ],

    openWindowEvent: 'openUsersWindow',
    addRecordText: 'Dodawanie pracownika',
    editRecordText: 'Edycja pracownika',
    getMyStore: function() { return this.getStore('Users'); }

});
