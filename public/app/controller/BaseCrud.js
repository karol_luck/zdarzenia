Ext.define('App.controller.BaseCrud', {
    extend: 'App.controller.WithPages',

    // To override
    openWindowEvent: 'none',
    addRecordText: 'Dodawanie rekordu',
    editRecordText: 'Edycja rekordu',
    getMyStore: function() { console.log('restStore is not implemented!'); },
    // ---

    init: function() {
        // Grab application events
        var me = this,
            store = this.getMyStore(),
            containerWindow = this.getModal().getXType(),
            formWindow = this.getForm().getXType();

        this.application.on(this.openWindowEvent, function() {
            me.getModal().show();
        });

        var controlObj = {};
        controlObj[containerWindow + ' #recordsList'] = {
            itemclick: function() { me._setItemBtns(true); },
            itemdblclick: function() { me.openEdit(); }
        };
        controlObj[containerWindow + ' #openAddRecord'] = {
            click: function() { console.log('OK!');  me.openAdd(); }
        };
        controlObj[containerWindow + ' #openEditRecord'] = {
            click: function() { me.openEdit(); }
        };
        controlObj[containerWindow + ' #openDeleteRecord'] = {
            click: function() { me.openDelete(); }
        };
        controlObj[formWindow + ' #confirmForm'] = {
            click: function() { me.confirmForm(); }
        };

        this.control(controlObj);
        this.callParent(arguments);
    },

    getContainerXType: function() {
        return this.getModal().getXType();
    },
    getPageNumber: function() {
        return this.getModal().down('#page-number');
    },

    getSelectedItem: function() {
        return this.getModal().down('#recordsList').getSelectionModel().getSelection()[0];
    },

    confirmDelete: function() {
        var me = this;
        this.getSelectedItem().destroy({
            callback: function() {
                me._loadStorePage();
            }
        });
    },
    confirmForm: function() {
        var formWindow = this.getForm(),
            form = formWindow.down('form').getForm(),
            store = this.getMyStore(),
            me = this;

        if(formWindow.adding) {
            store.add(form.getValues());
        }
        else {
            form.updateRecord();
        }

        if(!formWindow.adding && this.getSelectedItem().dirty != true) {
            formWindow.close();
            return;
        }

        formWindow.getEl().mask('Zapisywanie zmian...');
        store.sync({
            callback: function() {
                me._loadStorePage();
                formWindow.getEl().unmask();
                formWindow.close();
            }
        });
    },

    openDelete: function() {
        var me = this;
        Ext.MessageBox.confirm('Uwaga', 'Czy na pewno chcesz usunąć wybrany rekord?', function(choice) {
            if(choice == 'yes') {
                me.confirmDelete();
            }
        });
    },
    openAdd: function() {
        var formWindow = this.getForm();
        formWindow.setTitle(this.addRecordText);
        formWindow.adding = true;
        formWindow.show();
    },
    openEdit: function() {
        var selected = this.getSelectedItem(),
            formWindow = this.getForm();
        formWindow.setTitle(this.editRecordText);
        formWindow.adding = false;
        formWindow.down('form').getForm().loadRecord(selected);
        formWindow.show();
    },

    _setItemBtns: function(enabled) {
        var modal = this.getModal();
        modal.down('#openEditRecord').setDisabled(!enabled);
        modal.down('#openDeleteRecord').setDisabled(!enabled);
    }

});