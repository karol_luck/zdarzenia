Ext.define('App.controller.Main', {
    extend: 'Ext.app.Controller',
    views: ['Main.TopToolbar'],
    requires: ['Ext.window.MessageBox'],

    init: function() {
        var me = this;
        this.control({
            // Top Toolbar
            '#manageCarsBtn': {
                click: function() {
                    me.application.fireEvent('openCarsWindow');
                }
            },
            '#manageUsersBtn': {
                click: function() {
                    me.application.fireEvent('openUsersWindow');
                }
            },
            '#logoutBtn': {
                click: function() {
                    Ext.Msg.alert('Uwaga!', 'Funkcjonalność logowania nie została zaimplementowana.');
                }
            }
        });
    }
});
