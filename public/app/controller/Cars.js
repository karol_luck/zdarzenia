Ext.define('App.controller.Cars', {
    extend: 'App.controller.BaseCrud',
    views: ['Cars.MainWindow', 'Cars.Form'],
    stores: ['Cars'],

    refs: [
        {ref: 'modal', selector: 'CarsModal', xtype: 'carsModal', autoCreate: true},
        {ref: 'form', selector: 'CarsForm', xtype: 'carsForm', autoCreate: true}
    ],

    openWindowEvent: 'openCarsWindow',
    addRecordText: 'Dodawanie pojazdu',
    editRecordText: 'Edycja pojazdu',
    getMyStore: function() { return this.getStore('Cars'); }

});
