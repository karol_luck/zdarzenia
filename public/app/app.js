Ext.application({
    name: 'App',
    extend: 'Ext.app.Application',
    autoCreateViewport: true,

    controllers: ['Main', 'Events', 'Cars', 'Users']
});
