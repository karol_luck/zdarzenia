Ext.define('App.view.Main.TopToolbar', {
    extend: 'Ext.toolbar.Toolbar',
    alias: 'widget.topToolbar',

    requires:[
        'Ext.toolbar.TextItem'
    ],

    height: 32,
    items: [{
        xtype: 'tbtext',
        text: 'Przegląd zdarzeń drogowych',
        cls: 'app-title'
    }, '->', {
        xtype: 'button',
        itemId: 'manageUsersBtn',
        icon: '/img/icons/16px/users.png',
        text: 'Zarządzaj kierowcami'
    }, {
        xtype: 'button',
        itemId: 'manageCarsBtn',
        icon: '/img/icons/16px/truck.png',
        text: 'Zarządzaj flotą'
    }, '-', {
        xtype: 'button',
        icon: '/img/icons/16px/exit.png',
        itemId: 'logoutBtn',
        text: 'Wyloguj się'
    }]

});