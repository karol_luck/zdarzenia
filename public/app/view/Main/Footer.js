Ext.define('App.view.Main.Footer', {
    extend: 'Ext.toolbar.Toolbar',
    alias: 'widget.footer',
    cls: 'footer',

    buttonAlign: 'center',

    requires:[
        'Ext.toolbar.TextItem'
    ],

    layout: {
        type: 'hbox',
        pack: 'center'
    },

    items: [{
        xtype: 'tbtext',
        text: 'Copyright @ Przegląd zdarzeń drogowych. Projekt i wykonanie: Karol Łuckiewicz, na potrzeby procesu rekrutacji.'
    }]

});