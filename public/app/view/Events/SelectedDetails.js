Ext.define('App.view.Events.SelectedDetails', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.selectedDetails',

    title: 'Szczegóły zdarzenia',
    cls: 'details-table',
    margin: 5,
    layout: {
        type: 'table',
        columns: 2
    },
    defaults: {
        cls: 'show-data',
        border: false,
        layout: {
            type: 'table',
            columns: 2
        },
        defaults: {
            xtype: 'label',
            text: '---'
        }
    },
    items: [
        {
            xtype: 'fieldset', title: 'Podstawowe informacje', items: [
                {text: 'Data'}, {itemId: 'eventDate'},
                {text: 'Rodzaj'}, {itemId: 'eventType'}
            ]
        },
        {
            xtype: 'fieldset', title: 'Miejsce zdarzenia', items: [
                {text: 'Szer. Geograficzna'}, {itemId: 'eventLat'},
                {text: 'Wys. Geograficzna'}, {itemId: 'eventLng'}
            ]
        },
        {
            xtype: 'fieldset', title: 'Samochód', items: [
                {text: 'Marka'}, {itemId: 'eventCarBrand'},
                {text: 'Model'}, {itemId: 'eventCarModel'},
                {text: 'Numer rejestracyjny'}, {itemId: 'eventCarReg'}
            ]
        },
        {
            xtype: 'fieldset', title: 'Kierowca', items: [
                {text: 'Imie'}, {itemId: 'eventUserName'},
                {text: 'Nazwisko'}, {itemId: 'eventUserSurname'}
            ]
        }
    ],
    dockedItems: [{
        dock: 'bottom',
        border: false,
        style: {
            textAlign: 'right'
        },
        items: [{
            xtype: 'button',
            itemId: 'deleteBtn',
            disabled: true,
            icon: '/img/icons/16px/cross.png',
            text: 'Usuń zdarzenie'
        }]
    }]
});