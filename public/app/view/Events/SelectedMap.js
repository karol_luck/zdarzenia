Ext.define('App.view.Events.SelectedMap', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.selectedMap',

    title: 'Lokalizacja na mapie',
    layout: 'fit',

    items: [{
        xtype: 'gmappanel',
        zoomLevel: 12,
        id: 'mymap',
        gmapType: 'map',
        mapConfOpts: ['enableScrollWheelZoom','enableDoubleClickZoom','enableDragging'],
        mapControls: ['GSmallMapControl','GMapTypeControl','NonExistantControl'],
        setCenter: {
            lat: 54.3500,
            lng: 18.6667
        }
    }]
});