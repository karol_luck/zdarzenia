Ext.define('App.view.Events.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.eventsList',

    store: 'Events',

    title: "Lista zdarzeń",
    sortableColumns: false,
    columns: [{
        text: 'Typ',
        width: 30,
        dataIndex: 'type'
    }, {
        text: 'Data',
        flex: 2,
        dataIndex: 'date'
    }, {
        text: 'Samochód',
        flex: 4,
        renderer: function(v, p, record) {
            return record.get('carBrand') + ' ' + record.get('carModel') + ' (' + record.get('carReg') + ')';
        }
    }, {
        text: 'Kierowca',
        flex: 4,
        renderer: function(v, p, record) {
            return record.get('userName') + ' ' + record.get('userSurname');
        }
    }],

    dockedItems: [{
        xtype: 'toolbar',
        dock: 'bottom',
        items: ['->', {
            icon: '/img/icons/16px/first.png',
            itemId: 'page-first',
            disabled: true
        }, {
            icon: '/img/icons/16px/backward.png',
            itemId: 'page-backward',
            disabled: true
        }, {
            icon: '/img/icons/16px/previous.png',
            itemId: 'page-decrease',
            disabled: true
        }, {
            xtype: 'label',
            text: 'Strona',
            padding: 5
        }, {
            xtype: 'numberfield',
            itemId: 'page-number',
            minValue : 1,
            width: 50,
            cls: 'input-center',
            hideTrigger: true,
            allowDecimals : false,
            value: 1
        }, {
            icon: '/img/icons/16px/next.png',
            itemId: 'page-increase'
        }, {
            icon: '/img/icons/16px/forward.png',
            itemId: 'page-forward'
        }, {
            icon: '/img/icons/16px/last.png',
            itemId: 'page-last'
        }]
    }]
});