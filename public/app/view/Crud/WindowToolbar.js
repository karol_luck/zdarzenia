Ext.define('App.view.Crud.WindowToolbar', {
    extend: 'Ext.toolbar.Toolbar',
    alias: 'widget.crudWindowToolbar',

    items: [{
        icon: '/img/icons/16px/plus.png',
        itemId: 'openAddRecord',
        text: 'Dodaj'
    }, {
        icon: '/img/icons/16px/cog.png',
        itemId: 'openEditRecord',
        text: 'Edytuj',
        disabled: true
    }, {
        icon: '/img/icons/16px/cross.png',
        itemId: 'openDeleteRecord',
        text: 'Usuń',
        disabled: true
    }, '->', {
        icon: '/img/icons/16px/first.png',
        itemId: 'page-first'
    }, {
        icon: '/img/icons/16px/backward.png',
        itemId: 'page-backward'
    }, {
        icon: '/img/icons/16px/previous.png',
        itemId: 'page-decrease'
    }, {
        xtype: 'label',
        text: 'Strona',
        padding: 5
    }, {
        xtype: 'numberfield',
        itemId: 'page-number',
        minValue : 1,
        width: 50,
        cls: 'input-center',
        hideTrigger: true,
        allowDecimals : false,
        value: 1
    }, {
        icon: '/img/icons/16px/next.png',
        itemId: 'page-increase'
    }, {
        icon: '/img/icons/16px/forward.png',
        itemId: 'page-forward'
    }, {
        icon: '/img/icons/16px/last.png',
        itemId: 'page-last'
    }]
});