Ext.define('App.view.Main', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'App.view.Main.TopToolbar',
        'App.view.Main.Footer',
        'App.view.Events.List',
        'App.view.Events.SelectedDetails',
        'App.view.Events.SelectedMap'
    ],
    xtype: 'mainContainer',

    layout: {
        type: 'border'
    },

    items: [{
        region: 'north',
        xtype: 'topToolbar'
    }, {
        region: 'west',
        xtype: 'eventsList',
        width: '40%'
    }, {
        region: 'center',
        xtype: 'tabpanel',
        itemId: 'selectedTab',
        deferredRender: false,
        activeTab: 0,

        items: [{
            xtype: 'selectedDetails'
        }, {
            xtype: 'selectedMap'
        }]
    }, {
        region: 'south',
        xtype: 'footer'
    }]
});