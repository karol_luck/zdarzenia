Ext.define('App.view.Users.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.usersList',

    store: 'Users',
    sortableColumns: false,

    columns: [{
        text: 'Imie',
        flex: 3,
        dataIndex: 'name'
    }, {
        text: 'Nazwisko',
        flex: 3,
        dataIndex: 'surname'
    }, {
        text: 'Wiek',
        flex: 1,
        dataIndex: 'age'
    }]
});