Ext.define('App.view.Users.Form', {
    extend: 'Ext.window.Window',
    alias: 'widget.usersForm',

    modal: true,
    width: 300,
    height: 250,
    adding: false,

    layout: 'fit',

    items: [{
        xtype: 'form',
        defaults: {
            xtype: 'field',
            padding: 10
        },
        items: [{
            xtype: 'field',
            name : 'name',
            fieldLabel: 'Imie'
        }, {
            name: 'surname',
            fieldLabel: 'Nazwisko'
        }, {
            name: 'age',
            fieldLabel: 'Wiek'
        }],
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'bottom',
            items: [{
                text: 'Zapisz',
                itemId: 'confirmForm',
                icon: '/img/icons/16px/floppy.png'
            }]
        }]
    }]
});