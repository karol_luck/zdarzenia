Ext.define('App.view.Users.MainWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.usersModal',

    requires: ['App.view.Users.List', 'App.view.Crud.WindowToolbar'],

    title: 'Zarządzanie pracownikami',
    modal: true,
    width: 500,
    height: 503,

    layout: 'fit',

    items: [{
        xtype: 'usersList',
        itemId: 'recordsList',
        layout: 'fit',
        border: false
    }],

    dockedItems: [{
        dock: 'bottom',
        xtype: 'crudWindowToolbar'
    }]
});