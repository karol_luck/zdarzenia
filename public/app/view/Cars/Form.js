Ext.define('App.view.Cars.Form', {
    extend: 'Ext.window.Window',
    alias: 'widget.carsForm',

    modal: true,
    width: 300,
    height: 250,
    adding: false,

    layout: 'fit',

    items: [{
        xtype: 'form',
        defaults: {
            xtype: 'field',
            padding: 10
        },
        items: [{
            xtype: 'field',
            name : 'brand',
            fieldLabel: 'Marka'
        }, {
            name: 'model',
            fieldLabel: 'Model'
        }, {
            name: 'regNumber',
            fieldLabel: 'Numer rejestracyjny'
        }],
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'bottom',
            items: [{
                text: 'Zapisz',
                itemId: 'confirmForm',
                icon: '/img/icons/16px/floppy.png'
            }]
        }]
    }]
});