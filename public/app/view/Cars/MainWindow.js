Ext.define('App.view.Cars.MainWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.carsModal',

    requires: ['App.view.Cars.List', 'App.view.Crud.WindowToolbar'],

    title: 'Zarządzanie flotą',
    modal: true,
    width: 800,
    height: 503,

    layout: 'fit',

    items: [{
        xtype: 'carsList',
        itemId: 'recordsList',
        layout: 'fit',
        border: false
    }],

    dockedItems: [{
        dock: 'bottom',
        xtype: 'crudWindowToolbar'
    }]
});