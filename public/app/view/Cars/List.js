Ext.define('App.view.Cars.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.carsList',

    store: 'Cars',
    sortableColumns: false,

    columns: [{
        text: 'Marka',
        flex: 3,
        dataIndex: 'brand'
    }, {
        text: 'Model',
        flex: 3,
        dataIndex: 'model'
    }, {
        text: 'Numer rejestracyjny',
        flex: 2,
        dataIndex: 'regNumber'
    }]
});