Ext.define('App.model.User', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'name', type: 'string'},
        {name: 'surname', type: 'string'},
        {name: 'age', type: 'int'}
    ],

    proxy: {
        type: 'rest',
        url : '/rest/users/auto'
    }
});