Ext.define('App.model.Car', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'brand', type: 'string'},
        {name: 'model', type: 'string'},
        {name: 'regNumber', type: 'string'}
    ],

    proxy: {
        type: 'rest',
        url : '/rest/cars/auto'
    }
});