Ext.define('App.model.Event', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'date', type: 'string'},
        {name: 'lat', type: 'float'},
        {name: 'lng', type: 'float'},
        {name: 'type', type: 'string'},

        {name: 'carId', type: 'int'},
        {name: 'carBrand', type: 'string'},
        {name: 'carModel', type: 'string'},
        {name: 'carReg', type: 'string'},

        {name: 'userId', type: 'int'},
        {name: 'userName', type: 'string'},
        {name: 'userSurname', type: 'string'}
    ],

    proxy: {
        type: 'rest',
        url : '/rest/events/auto'
    }
});