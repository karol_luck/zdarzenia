<?php

interface IDataBase {

    public function find($name, $data, $order = null, $start = 0, $limit = 0);
    public function findById($name, $id);

    public function count($table);
    public function save($table, $id, $data);
    public function delete($table, $data);
    public function insert($table, $data);

    public function beginTransaction();
    public function commit();
    public function rollback();

    public function getDriver();

}