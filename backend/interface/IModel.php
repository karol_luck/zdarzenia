<?php

interface IModel {

    public function getId();
    public function getTable();
    public function getParams();
    public function getData();
    public function getDefaultOrder();

    public function save();
    public function delete();

}