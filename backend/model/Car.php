<?php

class Car extends Model {
    protected $attributes = array('brand', 'model', 'regNumber');
    protected $defaultOrder = 'regNumber ASC';
}