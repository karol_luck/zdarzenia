<?php

class User extends Model {
    protected $attributes = array('name', 'surname', 'age');
    protected $defaultOrder = 'surname ASC';
}