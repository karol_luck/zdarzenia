<?php

class Event extends Model {
    protected $attributes = array('date', 'lat', 'lng', 'type');
    protected $defaultOrder = '`date` DESC';
}