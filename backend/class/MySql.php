<?php

class MySql implements IDataBase {

    /**
     * @var PDO
     */
    private $pdo;

    function __construct() {
        $this->initDb();
    }

    public function count($table) {
        $results = $this->pdo->query("SELECT COUNT(*) AS total FROM `$table`");
        return $results->fetch(PDO::FETCH_ASSOC)['total'];
    }

    public function find($name, $data, $order = null, $start = 0, $limit = 0) {
        $start = (int)$start;
        $limit = (int)$limit;
        $where = $this->buildWhere($data);
        $prepareData = $this->prepareData($data);
        $orderBy = (!$order ? '' : " ORDER BY " . $order);
        $limitStr = ($limit == 0 ? '' : " LIMIT $start, $limit");
        $results = $this->pdo->prepare("SELECT * FROM `{$name}` WHERE {$where}{$orderBy}{$limitStr}");
        $results->execute($prepareData);
        return $results->fetchAll(PDO::FETCH_ASSOC);
    }

    public function findById($name, $id) {
        // TODO: Implement findById() method.
    }

    public function save($table, $id, $data) {
        $set = $this->buildSet($data);
        $prepareData = $this->prepareData($data);
        $results = $this->pdo->prepare("UPDATE `{$table}` SET {$set} WHERE `id` = $id");
        $results->execute($prepareData);
    }

    public function delete($table, $data) {
        $where = $this->buildWhere($data);
        $prepareData = $this->prepareData($data);
        $results = $this->pdo->prepare("DELETE FROM `{$table}` WHERE {$where}");
        $results->execute($prepareData);
    }

    public function insert($table, $data) {
        $insert = $this->buildInsert($data);
        $prepareData = $this->prepareData($data);
        $results = $this->pdo->prepare("INSERT INTO `{$table}` {$insert}");
        $results->execute($prepareData);
        return $this->pdo->lastInsertId();
    }

    public function beginTransaction() {
        $this->pdo->beginTransaction();
    }

    public function commit() {
        $this->pdo->commit();
    }

    public function rollback() {
        $this->pdo->rollBack();
    }

    public function getDriver() {
        return $this->pdo;
    }

    private function prepareData($data) {
        $ret = array();
        foreach($data AS $key => $value) {
            $ret[':' . $key] = $value;
        }
        return $ret;
    }

    private function buildInsert($data) {
        $dataToBuild = array();
        foreach($data AS $key => $value) {
            $dataToBuild["`$key`"] = ":$key";
        }
        $keys = implode(", ", array_keys($dataToBuild));
        $values = implode(", ", $dataToBuild);

        return "($keys) VALUES ($values)";
    }

    private function buildSet($data) {
        $set = "";
        foreach($data AS $key => $value) {
            $set .= " `$key` = :$key ,";
        }
        return substr($set, 0, -1);
    }

    private function buildWhere($data) {
        $params = array_keys($data);
        $where = " 1 ";
        foreach($params AS $param) {
            $where .= "AND `$param`=:$param ";
        }
        return $where;
    }

    private function initDb() {
        $db=array();
        require(BACKEND_DIR . 'config/db.php');
        try {
            $this->pdo = new PDO ($db['type'] . ':dbname=' . $db['name'] . ';host=' . $db['host'] . ';charset=utf8', $db['user'], $db['password'], array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
            ));
        }
        catch (Exception $e) {
            throw new PDOException('Blad polaczenia z baza danych');
        }
    }
}