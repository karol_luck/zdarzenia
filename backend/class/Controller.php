<?php

class Controller implements IController {

    /**
     * @var IDataBase
     */
    private $db;

    function __construct() {
        $this->loadDB();
    }

    public function getDb() {
        return $this->db;
    }

    private function loadDB() {
        $this->db = new MySql();
    }
}