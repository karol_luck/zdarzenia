<?php

class Model implements IModel {

    protected $attributes = array();
    protected $defaultOrder;

    protected $table;
    private $id;
    private $data = array();

    /**
     * @var IDataBase
     */
    private $db;

    /**
     * @param $db IDataBase
     */
    function __construct($db) {
        $this->db = $db;
        $this->loadTableName();
    }

    public function getId() {
        return $this->id;
    }

    public function getParams() {
        if(!array_key_exists('id', $this->attributes)) {
            $this->attributes[] = 'id';
        }
        return $this->attributes;
    }

    public function getDefaultOrder() {
        return $this->defaultOrder;
    }

    public function getData() {
        return $this->data;
    }

    public function getTable() {
        return $this->table;
    }

    public function save() {
        $this->db->save($this->table, $this->id, $this->getData());
    }

    public function delete() {
        $this->db->delete($this->table, array('id' => $this->getId()));
    }

    private function loadTableName() {
        if($this->table) return;
        $className = $this->toPlural(get_class($this));
        $this->table = strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $className));
    }

    private function toPlural($singular) {
        if(substr($singular, 0, -1) == 'y') {
            return substr($singular, -1) . 'ies';
        }
        return $singular . 's';
    }
}