<?php

class RestController extends Controller {
    protected $modelName = 'undefined';

    /**
     * @var IModel
     */
    protected $model;

    function __construct() {
        parent::__construct();
        $this->model = new $this->modelName($this->getDb());
    }

    public function getAction() {
        $found = $this->getDb()->find($this->model->getTable(), array(), $this->model->getDefaultOrder(), $_GET['start'], $_GET['limit']);
        $this->printJson($found, $this->model->getTable(), array('total' => $this->getDb()->count($this->model->getTable())));
    }

    public function deleteAction() {
        $id = (int)$_GET['id'];
        $this->getDb()->delete($this->model->getTable(), array('id' => $id));
        $this->printSuccess();
    }

    public function postAction() {
        $data = $this->getPayload();
        $isAdding = empty($data['id']);
        if($isAdding) {
            $insertId = $this->getDb()->insert($this->model->getTable(), $data);
            $data['id'] = $insertId;
            $this->printJson($data, $this->model->getTable());
        }
        else {
            $id = (int)$data['id'];
            $this->getDb()->save($this->model->getTable(), $id, $data);
            $this->printJson($data, $this->model->getTable());
        }
    }

    protected function getPayload() {
        return json_decode(file_get_contents('php://input'), true);
    }

    protected function printFail() {
        echo json_encode(array('success' => false));
    }

    protected function printSuccess() {
        echo json_encode(array('success' => true));
    }

    protected function printJson($data, $model = false, $append = array()) {
        if($model) {
            $data = array($model => $data);
        }
        if(!empty($append)) {
            foreach($append AS $key => $value) {
                $data[$key] = $value;
            }
        }
        echo json_encode($data);
    }
}