<?php

class EventsController extends RestController {
    protected $modelName = 'Event';

    function getAction() {
        $start = (int)$_GET['start'];
        $limit = (int)$_GET['limit'];

        // TODO: obsłużyć to relacjami zdefiniowanymi w modelach i za pomocą RestController::getAction()
        /**
         * @var $pdo PDO
         */
        $pdo = $this->getDb()->getDriver();
        $total = $this->getDb()->count($this->model->getTable());
        $results = $pdo->query("SELECT e.*,
                            c.id AS carId, c.brand AS carBrand, c.model AS carModel, c.regNumber AS carReg,
                            u.id AS userId, u.name AS userName, u.surname AS userSurname, u.age AS userAge
                        FROM `events` e
                        LEFT JOIN `cars` c ON (e.carId = c.id)
                        LEFT JOIN `users` u ON (e.userId = u.id)
                        ORDER BY `date` DESC
                        LIMIT {$start}, {$limit}");
        $this->printJson($results->fetchAll(PDO::FETCH_ASSOC), $this->model->getTable(), array('total' => $total));
    }

    function lastEventAction() {
        $last = $this->getDb()->getDriver()->query("SELECT MAX(id) AS lastId FROM `events`")->fetch();
        $this->printJson(array('lastId' => $last['lastId']));
    }
}