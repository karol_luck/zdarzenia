<?php

class CarsController extends RestController {
    protected $modelName = 'Car';

    function deleteAction() {
        /**
         * @var $pdo PDO
         */
        $pdo = $this->getDb()->getDriver();
        $id = (int)$_GET['id'];

        $this->getDb()->beginTransaction();
        try {
            $pdo->query("DELETE FROM `events` WHERE `carId`='{$id}'");
            parent::deleteAction();
            $pdo->commit();
        }
        catch(PDOException $e) {
            $this->getDb()->rollback();
            $this->printFail();
        }
    }
}